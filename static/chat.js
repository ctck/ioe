$(function() {
  var socket = io.connect('http://' + document.domain + ':' + location.port + '/chat');
  socket.on('announce', function(info) {
    $('#messageBox').append('<li class="w3-text-indigo">' + info.data + '</li>');
  });
  socket.on('message', function(msg) {
    $('#messageBox').append('<li><span class="sender">' + msg.sender + ': </span>' + msg.message + '</li>');
  });

  $('#messageInput').keypress(function(e) {
    if (e.which == 13 && $(this).text()) {
      socket.send($(this).text());
      $(this).text('');
    };
  });
});