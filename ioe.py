# App to solve this type of execise:
# Input: a sequence of character
# Output: eliminate a character in input so that the remain ones form
#         a meaningful word, return that word

from flask import Flask, request, url_for, redirect, render_template, session
from flask.ext.socketio import SocketIO, emit
import requests
import json
#import eventlet
#eventlet.monkey_patch()

app = Flask(__name__)
app.config['SECRET_KEY'] = 'thuonghoai'
io = SocketIO(app)

# Models
def query(word):
    params = {'from':'eng',
            'dest':'vie',
            'format':'json',
            'phrase':word.strip(),
            }
    return requests.get('http://glosbe.com/gapi/translate', params=params).json()

# Controllers
def check_word(word):
    if query(word).get('tuc'):
        return True
    else:
        return False
                
# Views
@app.route('/')
def home():
    return render_template('home.html', remove = session.get('remove',''),
                           fillin = session.get('fillin',''))

@app.route('/remove')
def remove():
    raw = request.args.get('remove')
    if raw:
        for i in range(len(raw)):
            word = raw[:i] + raw[i+1:]
            if check_word(word):
                session['remove'] = word
                break
        else:
            flash('no word match')
    return redirect(url_for('home'))

@app.route('/fillin')
def fillin():
    raw = request.args.get('fillin')
    if raw:
        if raw == 'flaskchat' or raw == 'passcode':
            session['chat'] = raw
            return redirect(url_for('chat'))
        for i in range(97, 123):
            word = raw.replace('*', chr(i))
            if check_word(word):
                session['fillin'] = word
                break
        else:
            flash('no word match')
    return redirect(url_for('home'))

@app.route('/chat')
def chat():
    return render_template('chat.html')

@io.on('connect', namespace='/chat')
def user_connect():
    emit('announce', dict(data=session['chat']+' connected!'), broadcast=True)

@io.on('disconnect', namespace='/chat')
def user_disconnect():
    emit('announce', dict(data=session['chat']+' disconnected!'), broadcast=True)

@io.on('message', namespace='/chat')
def display_message(message):
    emit('message', {'sender': session['chat'], 'message': message}, broadcast=True)

# Debug runtime
if __name__ == '__main__':
    #import socketio
    #import eventlet
    #app = socketio.Middleware(io, app)
    #eventlet.wsgi.server(eventlet.listen(('127.0.0.1', 5000)), app)
    io.run(app, debug=True)
